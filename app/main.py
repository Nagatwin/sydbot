#% Import the modules
from dotenv import load_dotenv
import os

import discord

#% Load the .env variables
load_dotenv()
DISCORD_TOKEN = os.getenv('DISCORD_TOKEN')

#% Create a discord client
client = discord.Client()

async def create_embed(message, create_command):
    #% Message content
    message_content = message.content
    if len(message_content) < len(create_command + ' '):
        #% No args
        await message.channel.send("Hey you need to enter the correct command. The correct command is '{} #channel | Title | Description'".format(create_command))
        return
    
    message_content = message_content[len(create_command) + 1:]
    
    #% Check arguments
    args = message_content.split('|')
    
    if len(args) < 3:
        #% No args
        await message.channel.send("Hey you need to enter the correct command. The correct command is '{} #channel | Title | Description'".format(create_command))
        return
    
    #% Retrieve channel id (like this <#721384066921791629>)
    channel = args[0]
    channel_id = None
    try:
        channel_id = int(channel.strip()[2:-1])
    except:
        await message.channel.send("Invalid channel. Please tag the channel correctly")
        return
    
    
    title = args[1]
    desc = '|'.join(args[2:])
    
    if len(desc) > 2048:
        #% No args
        await message.channel.send("Description is too long (2048 character max).")
        return
    
    embed = discord.Embed(
        title=title,
        colour=discord.Colour(0x77CDD9),
        description=desc,
    )
    
    channel_to_use = message.guild.get_channel(channel_id)
    
    m = await channel_to_use.send(embed=embed)
    embed = discord.Embed(
        colour=discord.Colour(0x77CDD9),
        description="Done, [link to message]({})".format(m.jump_url),
    )
    
    await message.channel.send(embed=embed)

async def edit_embed_title(message, edit_title_command):
    #% Message content
    message_content = message.content
    if len(message_content) < len(edit_title_command + ' '):
        #% No args
        await message.channel.send("Hey you need to enter the correct command. The correct command is '{} message_id new_title".format(edit_title_command))
        return
    
    message_content = message_content[len(edit_title_command) + 1:]
    
    #% Check arguments
    args = message_content.split(' ')
    
    if len(args) < 2:
        #% No args
        await message.channel.send("Hey you need to enter the correct command. The correct command is '{} message_id new_title".format(edit_title_command))
        return
    
    #% Retrieve message id
    message_to_edit_id = args[0]
    try:
        message_to_edit_id = int(message_to_edit_id.strip())
    except:
        await message.channel.send("Invalid message id.")
        return
    
    #% Title to set
    title = ' '.join(args[1:])
    
    #% Check every channel for the message id
    message_found = False
    for channel in message.guild.channels:
        try:
            msg_to_edit = await channel.fetch_message(message_to_edit_id)
            message_found = True
            
            embed = msg_to_edit.embeds[0]
            
            embed.title = title
            
            await msg_to_edit.edit(embed = embed)
            
            embed = discord.Embed(
                colour=discord.Colour(0x77CDD9),
                description="Done, [link to message]({})".format(msg_to_edit.jump_url),
            )
            
            await message.channel.send(embed=embed)
        except:
            continue
    
    #% Message not found; warn the user
    if not message_found:
        await message.channel.send("Message not found. Have you entered the right id?")

async def edit_embed_description(message, edit_description_command):
    #% Message content
    message_content = message.content
    if len(message_content) < len(edit_description_command + ' '):
        #% No args
        await message.channel.send("Hey you need to enter the correct command. The correct command is '{} message_id new_description".format(edit_description_command))
        return
    
    message_content = message_content[len(edit_description_command) + 1:]
    
    #% Check arguments
    args = message_content.split(' ')
    
    if len(args) < 2:
        #% No args
        await message.channel.send("Hey you need to enter the correct command. The correct command is '{} message_id new_description".format(edit_description_command))
        return
    
    #% Retrieve message id
    message_to_edit_id = args[0]
    try:
        message_to_edit_id = int(message_to_edit_id.strip())
    except:
        await message.channel.send("Invalid message id.")
        return
    
    #% Description to set
    description = ' '.join(args[1:])
    
    #% Check every channel for the message id
    message_found = False
    for channel in message.guild.channels:
        try:
            msg_to_edit = await channel.fetch_message(message_to_edit_id)
            message_found = True
            
            embed = msg_to_edit.embeds[0]
            
            embed.description = description
            
            await msg_to_edit.edit(embed = embed)
            
            embed = discord.Embed(
                colour=discord.Colour(0x77CDD9),
                description="Done, [link to message]({})".format(msg_to_edit.jump_url),
            )
            
            await message.channel.send(embed=embed)
        except:
            continue
    
    #% Message not found; warn the user
    if not message_found:
        await message.channel.send("Message not found. Have you entered the right id?")

async def edit_embed_image(message, edit_image_command):
    #% Message content
    message_content = message.content
    if len(message_content) < len(edit_image_command + ' '):
        #% No args
        await message.channel.send("Hey you need to enter the correct command. The correct command is '{} message_id new_image_url".format(edit_image_command))
        return
    
    message_content = message_content[len(edit_image_command) + 1:]
    
    #% Check arguments
    args = message_content.split(' ')
    
    if len(args) != 2:
        #% No args
        await message.channel.send("Hey you need to enter the correct command. The correct command is '{} message_id new_image_url".format(edit_image_command))
        return
    
    #% Retrieve message id
    message_to_edit_id = args[0]
    try:
        message_to_edit_id = int(message_to_edit_id.strip())
    except:
        await message.channel.send("Invalid message id.")
        return
    
    #% Image url to set
    image = args[1]
    
    #% Check every channel for the message id
    message_found = False
    for channel in message.guild.channels:
        try:
            #% Retrieve the message
            msg_to_edit = await channel.fetch_message(message_to_edit_id)
            message_found = True
            
            #% Get the embed
            embed = msg_to_edit.embeds[0]
            
            embed.set_image(url=image)
            
            #% Try editing it
            try:
                await msg_to_edit.edit(embed = embed)
                embed = discord.Embed(
                    colour=discord.Colour(0x77CDD9),
                    description="Done, [link to message]({})".format(msg_to_edit.jump_url),
                )
                
                await message.channel.send(embed=embed)
            #% Could not get the image from the URL
            except discord.errors.HTTPException as e:
                await message.channel.send("Wrong image URL.")
        except:
            continue
    
    #% Message not found; warn the user
    if not message_found:
        await message.channel.send("Message not found. Have you entered the right id?")

async def image_embed(message, image_command):
    #% Message content
    message_content = message.content
    if len(message_content) < len(image_command + ' '):
        #% No args
        await message.channel.send("Hey you need to enter the correct command. The correct command is '{} #channel | image_url".format(image_command))
        return
    
    message_content = message_content[len(image_command) + 1:]
    
    #% Check arguments
    args = message_content.split('|')
    
    if len(args) != 2:
        #% No args
        await message.channel.send("Hey you need to enter the correct command. The correct command is '{} #channel | image_url".format(image_command))
        return
    
    #% Retrieve channel id (like this <#721384066921791629>)
    channel = args[0]
    channel_id = None
    try:
        channel_id = int(channel.strip()[2:-1])
    except:
        await message.channel.send("Invalid channel. Please tag the channel correctly.")
        return
    
    image_url = args[1]
    
    embed = discord.Embed(colour=discord.Colour(0x77CDD9))
    embed.set_image(url=image_url)
    
    channel_to_use = message.guild.get_channel(channel_id)
    
    try:
        m = await channel_to_use.send(embed=embed)
        embed = discord.Embed(
            colour=discord.Colour(0x77CDD9),
            description="Done, [link to message]({})".format(m.jump_url),
        )
        
        await message.channel.send(embed=embed)
    except discord.errors.HTTPException as e:
        await message.channel.send("Wrong image URL.")


#% Print when the client is ready
@client.event
async def on_ready():
    await client.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name='mod plans'))
    print('We have logged in as {0.user}'.format(client))

#% This is our message handler
@client.event
async def on_message(message):
    if message.author == client.user:
        return

    print(message.content)

    #% Check the command
    if message.content.lower().startswith('pcove hello'):
        #% Retrieve author
        author = message.author

        #% Retrieve author name
        name_of_the_author = author.display_name

        #% Make a message to send
        message_to_send = "Hello {}, what a great day! :sun_with_face:".format(name_of_the_author)

        #% Send the message
        await message.channel.send(message_to_send)
        return
    
    if 'Moderators' not in [r.name for r in message.author.roles] and  'Modmins' not in [r.name for r in message.author.roles] and 'HostBot Manager' not in [r.name for r in message.author.roles] and 'Madmans' not in [r.name for r in message.author.roles]:
       return

    #% Check the command
    create_command = 'pcove create'
    if message.content.lower().startswith(create_command):
        await create_embed(message, create_command)
        return
        
    #% Check the command
    edit_title_command = 'pcove edit title'
    if message.content.lower().startswith(edit_title_command):
        await edit_embed_title(message, edit_title_command)
        return
        
    #% Check the command
    edit_description_command = 'pcove edit desc'
    if message.content.lower().startswith(edit_description_command):
        await edit_embed_description(message, edit_description_command)
        return
        
    #% Check the command
    edit_image_command = 'pcove edit image'
    if message.content.lower().startswith(edit_image_command):
        await edit_embed_image(message, edit_image_command)
        return
        
    #% Check the command
    image_command = 'pcove image'
    if message.content.lower().startswith(image_command):
        await image_embed(message, image_command)
        return

#% Using our token
client.run(DISCORD_TOKEN)
